import struct
import subprocess
import random


def generate_random_geneset():
    return [
        random.uniform(-1, 1),
        random.uniform(-1, 1),
        random.uniform(-1, 1),
        random.uniform(-1, 1),
        random.uniform(-1, 1),
        random.choice([True, False]),
        random.choice([True, False]),
    ]


def generate_first_population(count: int):
    population = []

    for i in range(count):
        population.append(generate_random_geneset())

    return population


def cross_float(a: float, b: float):
    return (a + b) / 2


def cross_boolean(a: bool, b: bool):
    return a ^ b


def write_genes(genes):
    with open("genes.txt", "w") as f:
        for i in genes:
            f.write(str(i).lower() + " ")


def write_genes_date(genes):
    with open(f"out/genes-p{population_size_init}-g{generations}-{datetime.datetime.now().isoformat()}.txt", "w") as f:
        for i in genes:
            f.write(str(i).lower() + " ")


def fitness(genes) -> int:
    write_genes(genes)
    return int(subprocess.check_output(["java",
                                        "-classpath",
                                        "/home/proc-daemon/IdeaProjects/SaveTheLastBullet/out/production/"
                                        "SaveTheLastBullet",
                                        "co.megadodo.koth.zombies.Main",
                                        "PlayerGenetic"]))


def run_game():
    subprocess.run(["java",
                    "-classpath",
                    "/home/proc-daemon/IdeaProjects/SaveTheLastBullet/out/production/"
                    "SaveTheLastBullet",
                    "co.megadodo.koth.zombies.Main"])


def breed(p1, p2):
    result = []

    for i in range(len(p1)):
        val = p1[i]
        if type(val) == float:
            result.append(cross_float(val, p2[i]))
        else:
            result.append(cross_boolean(val, p2[i]))

    return result


def flip_random_bit(param: bytes):
    number = int.from_bytes(param, "little")

    number = number ^ (1 << random.randint(1, number.bit_length()))

    while True:
        try:
            return number.to_bytes(8, "little")
        except:
            number = number ^ (1 << random.randint(1, number.bit_length()))


def mutate(p, mutation_chance):
    ret = []

    for i in p:
        temp = i

        if random.random() < mutation_chance:
            if type(temp) == float:
                temp = struct.unpack("@d", flip_random_bit(struct.pack("@d", temp)))[0]

                if temp > 1:
                    temp = 1
                elif temp < -1:
                    temp = -1
            else:
                temp = not temp

        ret.append(temp)

    return ret


def calculate_next_generation(population_sorted, best_sample, lucky_few):
    next_generation = []
    for i in range(best_sample):
        next_generation.append(population_sorted[i])
    for i in range(lucky_few):
        next_generation.append(random.choice(population_sorted))

    children = []
    for i in range(len(population_sorted) - best_sample - lucky_few):
        children.append(mutate(breed(*random.sample(next_generation, 2)), 0.01))

    return next_generation + children


if __name__ == "__main__":
    import datetime

    start = datetime.datetime.now()

    population_size_init = 30  # >= 6
    population = generate_first_population(population_size_init)
    print("Calculated initial generation")
    generations = 30

    for i in range(generations):
        population = calculate_next_generation(sorted(population, key=fitness, reverse=True),
                                               len(population) // 3,
                                               len(population) // 6)
        print(f"Calculated subsequent generation {i + 1}")

    best = max(population, key=fitness)

    end = datetime.datetime.now()

    print(f"In {end - start}, "
          f"{generations} generation(s) have been calculated. The final result is:")

    print(" ".join(map(str, best)))
    write_genes_date(best)
    write_genes(best)
    run_game()
